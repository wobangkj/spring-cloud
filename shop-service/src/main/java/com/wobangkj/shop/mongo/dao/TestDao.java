//package com.wobangkj.shop.mongo.dao;
//
//import com.wobangkj.shop.mongo.model.Test;
//
///**
// * test dao
// *
// * @author dreamlu
// */
//public interface TestDao {
//
//	void create(Test demoEntity);
//
//	void delete(String id);
//
//	void update(Test demoEntity);
//
//	Test id(String id);
//
//	//List<Test> search(Map<String, Object> params);
//}