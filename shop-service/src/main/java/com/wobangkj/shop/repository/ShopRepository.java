package com.wobangkj.shop.repository;

import com.wobangkj.shop.model.Shop;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

//@CacheConfig(cacheNames = "shops")
public interface ShopRepository extends JpaRepository<Shop, Long> {

	@Query(value = "select id from Shop where unionid=?1")
	List getIdByUnionid(String unionid);


	/**
	 * jpa 原生sql
	 *
	 * @param name
	 * @param pageable
	 * @return
	 */
	@Query(nativeQuery = true,
			value = "select * from shop " +
					"where name like CONCAT('%',:name,'%') ")
	Page<Shop> findByName(String name, Pageable pageable);
}
