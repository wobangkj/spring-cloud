package com.wobangkj.shop.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * (Admin)实体类
 *
 * @author dreamlu
 * @since 2019-06-19 15:13:38
 */
@Data
public class Admin implements Serializable {
    private static final long serialVersionUID = -95985894816253331L;
    
    private Long id;
    
    private String account;
    
    private String password;

}