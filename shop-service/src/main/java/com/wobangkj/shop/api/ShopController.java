package com.wobangkj.shop.api;

import com.wobangkj.shop.model.ShopDe;
import com.wobangkj.shop.service.ShopService;
import com.wobangkj.shop.model.Shop;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/shop")
public class ShopController {

	@Autowired
	private ShopService shopService;

	@PostMapping(value = "/create")
	public Object create(@RequestBody Shop data) {
		// 创建时间
		data.setCreatetime(Timestamp.valueOf(LocalDateTime.now()));
		return shopService.create(data);
	}

	@PutMapping(value = "/update")
	public Object update(@RequestBody Shop shop) {

		return shopService.update(shop);
	}

	@DeleteMapping(value = "/delete/{id}")
	public Object delete(@PathVariable(value = "id") Long id) {
		return shopService.delete(id);
	}

	@GetMapping("/id")
	public Object id(Long id) {

		return shopService.id(id);
	}


	@GetMapping("/search")
	public Object search(@RequestParam(required = false) Map<String, Object> params, Shop shop) {
		return shopService.search(params, shop);
	}

	@GetMapping("/idDe")
	public Object idDe(Long id) {

		return shopService.idDe(id);
	}

	@GetMapping("/searchDe")
	public Object searchDe(@RequestParam(required = false) Map<String, Object> params, ShopDe shop) {
		return shopService.searchDe(params, shop);
	}


	@GetMapping("/getId")
	public Object getIdByUnionid(String unionid) {
		return shopService.getIdByUnionid(unionid);
	}
}
