package com.wobangkj.shop;

import com.esotericsoftware.minlog.Log;
import com.wobangkj.shop.service.AdminService;
import com.wobangkj.shop.service.ShopService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ShopApplicationTests {

	@Autowired
	private ShopService shopService;

	@Resource
	private AdminService adminService;

	@Test
	public void test() {
		Object data = shopService.getShopByName("测试", 1, 3);
		Log.info("[返回数据]:" + data);

		Log.info(adminService.queryById(1L).getAccount());
	}

//	@Autowired
//	private TestDao testDao;
//
//	@Test
//	public void testMongo() {
//		com.wobangkj.shop.mongo.model.Test test = new com.wobangkj.shop.mongo.model.Test();
//		test.setName("测试2");
//		testDao.create(test);
//	}
//
//	@Test
//	public void findMongo(){
//
//		Log.info(testDao.id("5d10a4a97f7530109a0c646b").getName());
//	}
//
//	@Test
//	public void deleteMongo(){
//		testDao.delete("5d10a4a97f7530109a0c646b");
//	}
//
//	@Test
//	public void updateMongo(){
//		com.wobangkj.shop.mongo.model.Test test = new com.wobangkj.shop.mongo.model.Test();
//		test.setId("5d10a4a97f7530109a0c646b");
//		test.setName("测试v3");
//		testDao.update(test);
//	}
}
