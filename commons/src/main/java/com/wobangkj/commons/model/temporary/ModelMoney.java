package com.wobangkj.commons.model.temporary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 商家、员工id,金钱
 * 该项目临时具有
 * 放置临时目录下
 *
 * @author dreamlu
 * @date 2019/05/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelMoney {

	private Long id;

	private Double money;

	// 0增加, 1减少
	private Integer type;
}
