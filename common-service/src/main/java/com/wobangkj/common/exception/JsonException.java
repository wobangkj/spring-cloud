package com.wobangkj.common.exception;

//@ControllerAdvice
//@Slf4j
//public class JsonException {
//
//	// 配置文件信息
//	@Autowired
//	private Environment env;
//
//	// 文件大小超过指定大小，抛异常
//	@ResponseBody
//	@ExceptionHandler(value = MaxUploadSizeExceededException.class)
//	public Object resolveFileUploadException(MaxUploadSizeExceededException e) {
//		log.info("文件上传异常："+e.getMessage());
//
//		return Lib.GetMapData(Lib.CodeFile, "文件上传超过限制,建议"+ Objects.requireNonNull(env.getProperty("spring.servlet.multipart.max-file-size"))+"以下");
//	}
//
//	// 通用错误
//	@ResponseBody
//	@ExceptionHandler(value = Exception.class)
//	public Object exception(Exception e) {
//		log.info("未知错误："+e.getMessage());
//
//		return Lib.GetMapData(Lib.CodeFile, e.getMessage());
//	}
//}