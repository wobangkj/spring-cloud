package com.wobangkj.common.model.pca;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dreamlu
 * @date 2019/06/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityPinYin {

	// 首字母 key = title
	private String key;

	private String title;

	private List<City> items;
}
