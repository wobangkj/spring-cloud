package com.wobangkj.common.model.pca;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author dreamlu
 * @date 2019/06/14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class City {

	private String code;

	private String name;

	private List<Area> children;
}
