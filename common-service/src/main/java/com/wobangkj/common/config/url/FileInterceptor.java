package com.wobangkj.common.config.url;//package com.wobangkj.tool.config.url;
//
//import com.wobangkj.tool.util.file.ImageUtil;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.net.URLDecoder;
//import java.net.URLEncoder;
//import java.util.Optional;
//
////@Resource
//@Slf4j
//public class FileInterceptor extends HandlerInterceptorAdapter {
//
//	// 文件上传路径
//	private static final String UploadPath = "/common-service/static/file/";
//
//	/**
//	 * 在请求处理之前进行调用（Controller方法调用之前）
//	 * 基于URL实现的拦截器
//	 *
//	 * @param request
//	 * @param response
//	 * @param handler
//	 * @return
//	 */
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
//		// url test
//		//log.error("realURL:" + request.getAttribute("realURL"));
//
//		String width   = Optional.ofNullable(request.getParameter("width")).orElse("0");
//		String height  = Optional.ofNullable(request.getParameter("height")).orElse("0");
//		String quality = Optional.ofNullable(request.getParameter("quality")).orElse("1");
//
//		// 静态资源拦截, 分析
//		if (!width.equals("0") || !height.equals("0") || !quality.equals("1")) {
//			try {
//				String url = request.getRequestURL().toString();
//				log.error("[请求转发地址: {}]", url);
//				//log.error(response.isCommitted());
//				File rootFile = new File("");
//				// 文件名
//				String   fileName  = url.substring(url.lastIndexOf("/") + 1);
//				String[] fileNames = fileName.split("\\.");
//				fileName = URLDecoder.decode(fileNames[0], "utf-8") + "." + fileNames[1];
//				// 文件名判断
//				if (fileName.contains("-")) {
//					return true;
//				}
//				// 文件所在目录
//				String UpLoadPath  = rootFile.getAbsolutePath() + UploadPath;
//				String newFileName = width + "-" + height + "-" + quality + URLDecoder.decode(fileNames[0], "utf-8") + ".jpg";
//				File   src         = new File(UpLoadPath + newFileName);
//				// 解析后的文件存在判断
//				if (!src.exists()) {
//					// 不存在则创建
//					ImageUtil.Tosmallerpic(UpLoadPath, UpLoadPath, fileName, newFileName, Integer.parseInt(width), Integer.parseInt(height), Float.parseFloat(quality));
//				}
//				// 暂时固定
//				String newUrl = "/static/file/" + URLEncoder.encode(newFileName, "utf-8");
//				// 重置
//				request.getRequestDispatcher(newUrl).forward(request, response);
//				return false;
//			} catch (Exception e) {
//				log.error(e.getMessage());
//				return true;
//			}
//		}
//		return true;
//	}
//
////	@Override
////	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
////		System.out.println("处理请求后.......");
////		modelAndView = new ModelAndView("forword:"+request.getRequestURL());
////	}
////
////	@Override
////	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
////		System.out.println("请求处理完成......");
////		//response.sendRedirect(request.getRequestURI());
////		return;
////	}
//}